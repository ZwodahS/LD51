#!/bin/bash
ZF=$(haxelib libpath zf)
HEAPS=$(haxelib libpath heaps-dev)
ECHO=$(haxelib libpath echo)

function update() {
    pushd $ZF > /dev/null
    echo -n "zf: "
    git rev-parse HEAD
    popd > /dev/null

    pushd $HEAPS > /dev/null
    echo -n "heaps: "
    git rev-parse HEAD
    popd > /dev/null

    pushd $ECHO > /dev/null
    echo -n "echo: "
    git rev-parse HEAD
    popd > /dev/null
}

update > DEPENDENCIES.md
