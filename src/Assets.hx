/**
	Assets is used to store loaded assets
**/

import zf.ui.ScaleGridFactory;

class Assets {
	public static var bodyFont: hxd.res.BitmapFont;
	public static var bodyFonts: Array<h2d.Font>;

	public static var defaultFont: h2d.Font;
	public static var whiteBoxFactory: ScaleGridFactory;

	public static var res: ResourceManager;

	public static function load() {
		Assets.res = new ResourceManager();
		Assets.res.addSpritesheet(zf.Assets.loadAseSpritesheetConfig('graphics/packed.json'));

		final gluten = hxd.Res.load("fonts/gluten-medium.fnt").to(hxd.res.BitmapFont);
		Assets.bodyFont = gluten;
		Assets.bodyFonts = [
			gluten.toSdfFont(6, MultiChannel),
			gluten.toSdfFont(8, MultiChannel),
			gluten.toSdfFont(12, MultiChannel),
			gluten.toSdfFont(16, MultiChannel),
			gluten.toSdfFont(20, MultiChannel)
		];

		Assets.defaultFont = Assets.bodyFonts[0];

		Assets.whiteBoxFactory = new ScaleGridFactory(Assets.res.getTile("ui:whitebox"), 5, 5);
	}

	public static function fromColor(color: Color, width: Float, height: Float): h2d.Bitmap {
		final bm = new h2d.Bitmap(Assets.res.getTile("white"));
		bm.width = width;
		bm.height = height;
		bm.color.setColor(color);
		return bm;
	}
}
