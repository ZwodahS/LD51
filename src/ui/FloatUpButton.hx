package ui;

class FloatUpButton extends zf.ui.Button {
	var bitmap: h2d.Object;

	public var floatUpAmt: Float = -2;

	public function new(bm: h2d.Bitmap) {
		this.bitmap = bm;
		final bound = bitmap.getSize();
		super(Std.int(bound.width), Std.int(bound.height));
		this.addChild(bitmap);
	}

	override function updateButton() {
		if (this.isOver) {
			this.bitmap.y = this.floatUpAmt;
		} else {
			this.bitmap.y = 0;
		}
	}
}
