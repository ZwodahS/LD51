package ui;

import zf.ui.ScaleGridFactory;

class TooltipWindow extends zf.ui.Window {
	public var bgFactory: ScaleGridFactory;

	public var title: String;
	public var titleFont: String = "body.2";
	public var tooltip: String;
	public var tooltipFont: String = "body.1";

	public var minWidth: Int = 0;
	public var maxWidth: Int = 300;
	public var minHeight: Null<Int> = 50;

	public var spacing(default, set): Float = 0;

	public var text: h2d.HtmlText;

	public function set_spacing(f: Float): Float {
		this.spacing = f;
		if (this.text != null) this.text.lineSpacing = f;
		return f;
	}

	public function new(tooltip: String = null, title: String = null) {
		super();
		this.bgFactory = Assets.whiteBoxFactory;
		this.title = title;
		this.tooltip = tooltip;
	}

	dynamic public function getTooltip(): String {
		return this.tooltip;
	}

	dynamic public function getTitle(): String {
		return this.title;
	}

	override public function onShow() {
		this.removeChildren();
		var tooltipString = getTooltip();
		if (tooltipString == null) {
			this.visible = false;
			return;
		}

		final flow = Globals.uiBuilder.makeObjectFromXMLString('<layout-vflow spacing="10"></layout-vflow>');
		final title = this.getTitle();
		if (title != null) {
			final text = Globals.uiBuilder.makeObjectFromXMLString('
				<underline color="brown">
					<text fontName="${this.titleFont}" textColor="brown">${title}</text>
				</underline>
			');
			flow.addChild(text);
		}

		final text = Globals.uiBuilder.makeObjectFromXMLString('
			<text fontName="${this.tooltipFont}" textColor="black" maxWidth="${this.maxWidth - 30}">${tooltipString}</text>
		');
		flow.addChild(text);
		this.text = cast(text, h2d.HtmlText);
		this.text.lineSpacing = this.spacing;

		final obj = Globals.uiBuilder.makeObjectFromStruct({
			type: "window",
			conf: {
				item: {
					type: "object",
					conf: {
						object: flow,
					}
				},
				bgFactory: this.bgFactory,
				minWidth: this.minWidth,
				maxWidth: this.maxWidth,
			}
		});
		this.addChild(obj);
		this.visible = true;
	}
}
