package ui.components;

class Window extends zf.ui.builder.Component {
	public function new() {
		super("window");
	}

	override public function makeFromXML(element: Xml, context: BuilderContext): h2d.Object {
		final access = zf.Access.xml(element);

		final item = element.firstElement();
		if (item == null) return null;

		final minWidth = access.getInt("minWidth", 450);
		final maxWidth = access.getInt("maxWidth", null);

		final object = context.makeObjectFromXMLElement(item);

		return wrapObjectWithBackground(object, Assets.whiteBoxFactory, minWidth, maxWidth, null);
	}

	override public function makeFromStruct(c: Dynamic, context: BuilderContext): h2d.Object {
		final access = zf.Access.struct(c);

		var bgFactory: zf.ui.ScaleGridFactory = access.get("bgFactory");
		if (bgFactory == null) bgFactory = Assets.whiteBoxFactory;

		final item = context.makeObjectFromStruct(access.get("item"));
		if (item == null) return null;

		final minWidth = access.getInt("minWidth", 450);
		final maxWidth = access.getInt("maxWidth", null);

		return wrapObjectWithBackground(item, bgFactory, minWidth, maxWidth, null);
	}

	public static function wrapObjectWithBackground(object: h2d.Object, bgFactory: ScaleGridFactory = null,
			minWidth: Null<Int> = null, maxWidth: Null<Int> = null, minHeight: Null<Int> = null): h2d.Object {
		if (bgFactory == null) bgFactory = Assets.whiteBoxFactory;
		return _wrap(object, bgFactory, Constants.WindowPadding, minWidth, maxWidth, minHeight);
	}

	static function _wrap(object: h2d.Object, bgFactory: ScaleGridFactory, padding: Recti, minWidth: Null<Int> = null,
			maxWidth: Null<Int> = null, minHeight: Null<Int> = null): h2d.Object {
		final bounds = object.getBounds();
		final windowSize = Utils.getWindowSize(bounds, minWidth, maxWidth, minHeight);
		final obj = new h2d.Object();
		obj.addChild(bgFactory.make(windowSize));
		obj.addChild(object);
		object.x = padding.xMin;
		object.y = padding.yMin;
		return obj;
	}
}
