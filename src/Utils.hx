/**
	Utils store all the functions that generally useful for the whole repo, but not yet lib/common level.

	For the globals variable, see Globals.hx
	For constants, see Constants.hx

**/
class Utils {
	public static function stackItemToString(s: haxe.CallStack.StackItem) {
		switch (s) {
			case Module(m):
				return '${m}';
			case FilePos(s, file, line, _):
				if (s == null) {
					return '${file}:${line}';
				} else {
					return '${stackItemToString(s)} (${file}:${line})';
				}
			case Method(cn, method):
				if (cn == null) return '${method}';
				return '${cn}.${method}';
			case LocalFunction(v):
				return '$' + '${v}';
			case CFunction:
				return 'CFunction';
		}
	}

	public static function getWindowSize(bounds: h2d.col.Bounds, minWidth: Null<Int> = null,
			maxWidth: Null<Int> = null, minHeight: Null<Int> = null): Point2i {
		final windowSize: Point2i = [Std.int(bounds.width), Std.int(bounds.height)];
		windowSize.x += Constants.WindowPadding.xMin + Constants.WindowPadding.xMax;
		windowSize.y += Constants.WindowPadding.yMin + Constants.WindowPadding.yMax;
		windowSize.x = Math.clampI(windowSize.x, minWidth, maxWidth);
		windowSize.y = Math.clampI(windowSize.y, minHeight, null);
		return windowSize;
	}

	public static function getDiscordBtn(text: String): h2d.Object {
		final obj = new h2d.Object();
		final discordBtn = new FloatUpButton(Assets.res.getBitmap("button:discord"));
		discordBtn.onLeftClick = function() {
			hxd.System.openURL("https://discord.gg/BsNrBVf8ev");
		}
		obj.addChild(discordBtn);
		final t = new h2d.HtmlText(Assets.bodyFonts[1]);
		t.text = text;
		t.textColor = 0xfffffbe5;
		t.dropShadow = {
			dx: 1,
			dy: 1,
			color: 0xff1a1212,
			alpha: .4
		};
		obj.addChild(t.putOnRight(discordBtn, [4, 0]).centerYWithin(discordBtn));
		return obj;
	}
}
