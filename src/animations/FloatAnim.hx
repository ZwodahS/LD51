package animations;

/**
	Animate float up and down
**/
class FloatAnim {
	public var obj: h2d.Object;
	public var currentFloat: Float;
	public var currentDirection: Int;

	public var speed: Float;
	public var floatDistance: Float;

	public function new(obj: h2d.Object, floatDistance: Float, speed: Float) {
		this.obj = obj;
		this.currentFloat = 0;
		this.floatDistance = floatDistance;
		this.currentDirection = Math.sign(floatDistance);
		this.speed = Math.abs(speed);
	}

	public function update(dt: Float) {
		var current = this.currentFloat + (dt * this.currentDirection * speed);
		if (this.floatDistance > 0) {
			if (this.currentDirection > 0) {
				if (current >= this.floatDistance) {
					this.currentDirection = -1;
					current -= (current - this.floatDistance);
				}
			} else {
				if (current <= 0) {
					this.currentDirection = 1;
					current = -current;
				}
			}
		} else {
			if (this.currentDirection > 0) {
				if (current >= 0) {
					this.currentDirection = -1;
					current = -current;
				}
			} else {
				if (current <= this.floatDistance) {
					this.currentDirection = 1;
					current -= (current - this.floatDistance);
				}
			}
		}
		this.obj.y = this.obj.y - this.currentFloat + current;
		this.currentFloat = current;
	}
}
