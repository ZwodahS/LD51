package world;

import world.Faction;

class WorldState {
	public var r: hxd.Rand;

	// ---- Id generation ---- //

	/**
		Int counter for id generation
	**/
	var intCounter: zf.IntCounter.SimpleIntCounter;

	public var nextId(get, never): Int;

	public var player: Player;

	public var bullets: Array<Bullet>;

	public var food: Array<Food>;

	public var entities: zf.engine2.Entities<Entity>;

	public var entitiesByFaction: Map<Faction, Array<Entity>>;

	public var level: Int = 0;

	public var score: Int = 0;

	public function get_nextId(): Int {
		return this.intCounter.getNextInt();
	}

	public function new(seed: Int = 0) {
		this.intCounter = new zf.IntCounter.SimpleIntCounter();
		this.r = new hxd.Rand(seed);
		this.entities = new zf.engine2.Entities<Entity>();
		this.entitiesByFaction = new Map<Faction, Array<Entity>>();
		this.entitiesByFaction[FTPlayer] = [];
		this.entitiesByFaction[FTEnemy] = [];
		this.entitiesByFaction[FTNeutralPassive] = [];
		this.entitiesByFaction[FTNeutralHostile] = [];
		this.entitiesByFaction[FTPowerup] = [];
		this.bullets = [];
		this.food = [];
	}

	public function addEntity(e: Entity) {
		this.entities.add(e);
		if (e.typeId == "bullet") this.bullets.push(cast e);
		this.entitiesByFaction[e.faction].push(e);
	}

	public function removeEntity(e: Entity) {
		this.entities.remove(e);
		if (e.typeId == "bullet") this.bullets.remove(cast e);
		this.entitiesByFaction[e.faction].remove(e);
	}

	public static function newGame(): WorldState {
		final state = new WorldState(Random.int(0, zf.Constants.SeedMax));
		state.player = new Player(state.nextId);
		state.player.setPosition(Constants.WorldSize.x / 4, 180);
		state.player.health.current = 5000;
		state.addEntity(state.player);

		// add north wall
		final wall = new Wall(state.nextId, Constants.WorldSize.x, 16);
		wall.setPosition(Constants.WorldSize.x / 2, 8);
		state.addEntity(wall);

		// add west wall
		final wall = new Wall(state.nextId, 16, Constants.WorldSize.y);
		wall.setPosition(8, Constants.WorldSize.y / 2);
		state.addEntity(wall);

		// add south wall
		final wall = new Wall(state.nextId, Constants.WorldSize.x, 16);
		wall.setPosition(Constants.WorldSize.x / 2, Constants.WorldSize.y - 8);
		state.addEntity(wall);

		// add east wall
		final wall = new Wall(state.nextId, 16, Constants.WorldSize.y);
		wall.setPosition(Constants.WorldSize.x - 8, Constants.WorldSize.y / 2);
		state.addEntity(wall);

		return state;
	}
}
