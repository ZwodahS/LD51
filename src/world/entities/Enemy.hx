package world.entities;

class EnemyAIComponent extends AIComponent {
	public var moveTowardPlayers: Bool = true;
	public var moveSpeed: Float = 20;
	public var moveDelay: Float = 10 * .016; // calculate every .5 s

	var _moveDelay: Float = 0;
	var _currentVec: Point2f = [0, 0];

	public function new() {
		super();
	}

	override public function doNext(w: World, dt: Float) {
		// check if I want to move
		final player = w.worldState.player;

		var shouldMoveToPlayer = this.moveTowardPlayers;
		if (this.entity.bulletWeapon != null) {
			if (this.entity.bulletWeapon.isReady) {
				var shouldShoot = false;
				if (this.entity.bulletWeapon.attackRange == null) {
					shouldShoot = true;
				} else {
					final distance = hxd.Math.pow(this.entity.x - player.x, 2)
						+ hxd.Math.pow(this.entity.y - player.y, 2);
					if (distance <= hxd.Math.pow(this.entity.bulletWeapon.attackRange, 2)) {
						shouldShoot = true;
					}
				}
				if (shouldShoot == true) {
					w.dispatcher.dispatch(new MDoFireBullet(this.entity, player.x, player.y));
					shouldMoveToPlayer = false;
				}
			}
		}
		if (shouldMoveToPlayer) {
			this._moveDelay -= dt;
			if (this._moveDelay <= 0) {
				this._moveDelay = this.moveDelay;
				_moveTowardPlayer(w, dt);
			}
			this.entity.echo.body.velocity = [this._currentVec.x, this._currentVec.y];
		} else {
			this.entity.echo.body.velocity = [0, 0];
		}
	}

	function _moveTowardPlayer(w: World, dt: Float) {
		final player = w.worldState.player;
		// get move vector
		this._currentVec.xy(player.x - this.entity.x, player.y - this.entity.y);
		this._currentVec.normalize().scale(this.moveSpeed);
	}
}

class Enemy extends Entity {
	public var hp: zf.ui.Bar;

	function new(id: Int) {
		super(id);
		this.typeId = "enemy";
		this.faction = FTEnemy;
	}

	override public function update(dt: Float) {
		super.update(dt);
		if (this.health.current != this.hp.value) {
			this.hp.maxValue = this.health.max;
			this.hp.value = this.health.current;
			this.hp.visible = true;
		}
	}

	public static function makeRedMage(id: Int): Enemy {
		final enemy = new Enemy(id);

		enemy.render = new RenderComponent();
		var shadow = Assets.res.getBitmap("shadow:small");
		shadow.x = -8;
		shadow.y = -7;
		enemy.render.main.addChild(shadow);

		final bm = Assets.res.getAnim("entity:firemage");
		bm.speed = 4;
		bm.x = -9;
		bm.y = -9;
		enemy.render.main.addChild(bm);
		enemy.render.flip = true;
		enemy.echo = new EchoComponent(new echo.Body({
			mass: 10000,
			material: {
				elasticity: 1.0,
			},
			shape: {
				type: CIRCLE,
				radius: 6,
			}
		}));

		enemy.health = new HealthComponent();
		enemy.health.max = 1000;
		enemy.health.current = 1000;

		enemy.hp = new zf.ui.Bar(Normal, 0xfff2baac, null, 0, 16, 2);
		enemy.hp.x = -8;
		enemy.hp.y = -10;
		enemy.hp.maxValue = enemy.health.max;
		enemy.hp.value = enemy.health.current;
		// enemy.hp.visible = false;
		enemy.render.hud.addChild(enemy.hp);

		enemy.ai = new EnemyAIComponent();
		enemy.bulletWeapon = new BulletWeaponComponent();
		enemy.bulletWeapon.baseAttackSpeed = 1;
		enemy.bulletWeapon.damage = 1000;
		enemy.bulletWeapon.bulletSpeed = 100;
		enemy.bulletWeapon.bmId = "entity:bullet:fire";
		enemy.bulletWeapon.radius = 3;
		enemy.bulletWeapon.makeBullet = (id, target) -> {
			final bullet = new Bullet(id, FTEnemy, null, enemy.bulletWeapon.radius, null,
				Assets.res.getBitmap("entity:bullet:fire"));
			bullet.bm.x = -3;
			bullet.bm.y = -6;
			bullet.damage = enemy.bulletWeapon.damage;
			return bullet;
		}

		return enemy;
	}

	public static function makeBlueMage(id: Int): Enemy {
		final enemy = new Enemy(id);

		enemy.render = new RenderComponent();
		var shadow = Assets.res.getBitmap("shadow:small");
		shadow.x = -8;
		shadow.y = -7;
		enemy.render.main.addChild(shadow);

		final bm = Assets.res.getAnim("entity:icemage");
		bm.speed = 4;
		bm.x = -9;
		bm.y = -9;
		enemy.render.main.addChild(bm);
		enemy.render.flip = true;
		enemy.echo = new EchoComponent(new echo.Body({
			mass: 10000,
			material: {
				elasticity: 1.0,
			},
			shape: {
				type: CIRCLE,
				radius: 6,
			}
		}));

		enemy.health = new HealthComponent();
		enemy.health.max = 1000;
		enemy.health.current = 1000;

		enemy.hp = new zf.ui.Bar(Normal, 0xfff2baac, null, 0, 16, 2);
		enemy.hp.x = -8;
		enemy.hp.y = -10;
		enemy.hp.maxValue = enemy.health.max;
		enemy.hp.value = enemy.health.current;
		// enemy.hp.visible = false;
		enemy.render.hud.addChild(enemy.hp);

		enemy.ai = new EnemyAIComponent();
		enemy.bulletWeapon = new BulletWeaponComponent();
		enemy.bulletWeapon.baseAttackSpeed = 2;
		enemy.bulletWeapon.damage = 200;
		enemy.bulletWeapon.bulletSpeed = 50;
		enemy.bulletWeapon.bmId = "entity:bullet:ice";
		enemy.bulletWeapon.radius = 3;
		final frozen: Buff = new Buff(Frozen, 1);
		enemy.bulletWeapon.makeBullet = (id, target) -> {
			final bullet = new Bullet(id, FTEnemy, null, enemy.bulletWeapon.radius, null,
				Assets.res.getBitmap("entity:bullet:ice"));
			bullet.onHitBuff = frozen;
			bullet.bm.x = -3;
			bullet.bm.y = -3;
			bullet.damage = enemy.bulletWeapon.damage;
			return bullet;
		}

		return enemy;
	}

	public static function makeFarmer(id: Int): Enemy {
		final enemy = new Enemy(id);

		enemy.render = new RenderComponent();
		var shadow = Assets.res.getBitmap("shadow:small");
		shadow.x = -8;
		shadow.y = -7;
		enemy.render.main.addChild(shadow);

		final bm = Assets.res.getAnim("entity:farmer");
		bm.speed = 4;
		bm.x = -9;
		bm.y = -9;
		enemy.render.main.addChild(bm);
		enemy.render.flip = true;
		enemy.echo = new EchoComponent(new echo.Body({
			mass: 10000,
			material: {
				elasticity: 1.0,
			},
			shape: {
				type: CIRCLE,
				radius: 6,
			}
		}));

		enemy.health = new HealthComponent();
		enemy.health.max = 3000;
		enemy.health.current = 3000;

		enemy.hp = new zf.ui.Bar(Normal, 0xfff2baac, null, 0, 16, 2);
		enemy.hp.x = -8;
		enemy.hp.y = -10;
		enemy.hp.maxValue = enemy.health.max;
		enemy.hp.value = enemy.health.current;
		// enemy.hp.visible = false;
		enemy.render.hud.addChild(enemy.hp);

		final ai = new EnemyAIComponent();
		ai.moveSpeed = 40;
		enemy.ai = ai;

		enemy.bulletWeapon = new BulletWeaponComponent();
		enemy.bulletWeapon.baseAttackSpeed = 0.5;
		enemy.bulletWeapon.damage = 500;
		enemy.bulletWeapon.bulletSpeed = 400;
		enemy.bulletWeapon.attackRange = 50;
		enemy.bulletWeapon.bmId = "entity:pitchfork";
		enemy.bulletWeapon.radius = 4;
		enemy.bulletWeapon.maxRange = 70;
		enemy.bulletWeapon.makeBullet = (id, target) -> {
			final bullet = new Bullet(id, FTEnemy, null, enemy.bulletWeapon.radius, null,
				Assets.res.getBitmap("entity:pitchfork"));
			bullet.bm.x = -5;
			bullet.bm.y = -5;
			bullet.damage = enemy.bulletWeapon.damage;
			return bullet;
		}

		return enemy;
	}

	public static function makeArcher(id: Int): Enemy {
		final enemy = new Enemy(id);

		enemy.render = new RenderComponent();
		var shadow = Assets.res.getBitmap("shadow:small");
		shadow.x = -8;
		shadow.y = -7;
		enemy.render.main.addChild(shadow);

		final bm = Assets.res.getAnim("entity:archer");
		bm.speed = 4;
		bm.x = -9;
		bm.y = -9;
		enemy.render.main.addChild(bm);
		enemy.render.flip = true;
		enemy.echo = new EchoComponent(new echo.Body({
			mass: 10000,
			material: {
				elasticity: 1.0,
			},
			shape: {
				type: CIRCLE,
				radius: 6,
			}
		}));

		enemy.health = new HealthComponent();
		enemy.health.max = 1000;
		enemy.health.current = 1000;

		enemy.hp = new zf.ui.Bar(Normal, 0xfff2baac, null, 0, 16, 2);
		enemy.hp.x = -8;
		enemy.hp.y = -10;
		enemy.hp.maxValue = enemy.health.max;
		enemy.hp.value = enemy.health.current;
		// enemy.hp.visible = false;
		enemy.render.hud.addChild(enemy.hp);

		final ai = new EnemyAIComponent();
		ai.moveSpeed = 1;
		enemy.ai = ai;

		enemy.bulletWeapon = new BulletWeaponComponent();
		enemy.bulletWeapon.baseAttackSpeed = 0.25;
		enemy.bulletWeapon.damage = 1000;
		enemy.bulletWeapon.bulletSpeed = 450;
		enemy.bulletWeapon.attackRange = 600;
		enemy.bulletWeapon.bmId = "entity:arrow";
		enemy.bulletWeapon.radius = 2;
		enemy.bulletWeapon.makeBullet = (id, target) -> {
			final bullet = new Bullet(id, FTEnemy, null, enemy.bulletWeapon.radius, null,
				Assets.res.getBitmap("entity:arrow"));
			bullet.bm.x = -5;
			bullet.bm.y = -5;
			bullet.damage = enemy.bulletWeapon.damage;
			return bullet;
		}

		return enemy;
	}

	public static function makeGuard(id: Int): Enemy {
		final enemy = new Enemy(id);

		enemy.render = new RenderComponent();
		var shadow = Assets.res.getBitmap("shadow:small");
		shadow.x = -8;
		shadow.y = -7;
		enemy.render.main.addChild(shadow);

		final bm = Assets.res.getAnim("entity:guard");
		bm.speed = 4;
		bm.x = -9;
		bm.y = -9;
		enemy.render.main.addChild(bm);
		enemy.render.flip = true;
		enemy.echo = new EchoComponent(new echo.Body({
			mass: 10000,
			material: {
				elasticity: 1.0,
			},
			shape: {
				type: CIRCLE,
				radius: 6,
			}
		}));

		enemy.health = new HealthComponent();
		enemy.health.max = 10000;
		enemy.health.current = 10000;

		enemy.hp = new zf.ui.Bar(Normal, 0xfff2baac, null, 0, 16, 2);
		enemy.hp.x = -8;
		enemy.hp.y = -10;
		enemy.hp.maxValue = enemy.health.max;
		enemy.hp.value = enemy.health.current;
		// enemy.hp.visible = false;
		enemy.render.hud.addChild(enemy.hp);

		final ai = new EnemyAIComponent();
		ai.moveSpeed = 10;
		enemy.ai = ai;

		enemy.bulletWeapon = new BulletWeaponComponent();
		enemy.bulletWeapon.baseAttackSpeed = 0.5;
		enemy.bulletWeapon.damage = 1500;
		enemy.bulletWeapon.bulletSpeed = 400;
		enemy.bulletWeapon.attackRange = 30;
		enemy.bulletWeapon.bmId = "entity:sword";
		enemy.bulletWeapon.radius = 4;
		enemy.bulletWeapon.maxRange = 70;
		enemy.bulletWeapon.makeBullet = (id, target) -> {
			final bullet = new Bullet(id, FTEnemy, null, enemy.bulletWeapon.radius, null,
				Assets.res.getBitmap("entity:sword"));
			bullet.bm.x = -5;
			bullet.bm.y = -5;
			bullet.damage = enemy.bulletWeapon.damage;
			return bullet;
		}

		return enemy;
	}
}
