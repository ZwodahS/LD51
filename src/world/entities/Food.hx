package world.entities;

class Food extends Entity {
	public var foodValue: Int;

	public var foodType: FoodType;

	public var foodBm: h2d.Bitmap;

	public var floatAnim: animations.FloatAnim;

	public function new(id: Int, foodType: FoodType) {
		super(id);
		this.typeId = "food";
		this.foodType = foodType;
		this.foodValue = 5000;
		this.faction = FTPowerup;

		this.render = new RenderComponent();

		var shadow = Assets.res.getBitmap("shadow:small");
		shadow.x = -8;
		shadow.y = -6;

		var bm: h2d.Bitmap = Assets.res.getBitmap(this.foodType);
		switch (this.foodType) {
			case Carrot:
			case Cabbage:
			case Potato:
			case Apple:
			case Orange:
			case Cherry:
			case Banana:
			default:
		}
		bm.x = -8;
		bm.y = -8;
		this.render.object.addChild(shadow);
		this.foodBm = bm;
		this.render.object.addChild(bm);

		this.floatAnim = new animations.FloatAnim(this.foodBm, 3, 3);

		this.echo = new EchoComponent(new echo.Body({
			shape: {
				type: CIRCLE,
				radius: 16,
			}
		}), false);
	}

	public static function randomFood(id: Int, r: hxd.Rand): Food {
		final ft: FoodType = switch (r.randomInt(7)) {
			case 0: Carrot;
			case 1: Cabbage;
			case 2: Potato;
			case 3: Apple;
			case 4: Orange;
			case 5: Cherry;
			case 6: Banana;
			default: Carrot;
		}

		return new Food(id, ft);
	}

	override public function update(dt: Float) {
		super.update(dt);
		if (this.world != null) dt = this.world.worldSpeed * dt;
		if (this.floatAnim != null) this.floatAnim.update(dt);
	}
}
