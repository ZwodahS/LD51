package world.entities;

class Wall extends Entity {
	public function new(id: Int, width: Float, height: Float) {
		super(id);
		this.typeId = "wall";

		this.render = new RenderComponent();
		final object = new h2d.Object();
		for (x in 0...Std.int(width / 16)) {
			for (y in 0...Std.int(height / 16)) {
				final bm = Assets.res.getBitmap("entity:wall");
				bm.x = x * 16;
				bm.y = y * 16;
				object.addChild(bm);
			}
		}
		object.x = -width / 2;
		object.y = -height / 2;
		render.object.addChild(object);

		this.echo = new EchoComponent(new echo.Body({
			mass: 0,
			shape: {
				type: RECT,
				width: width,
				height: height,
			}
		}));
	}

	override public function update(dt: Float) {
		super.update(dt);
	}
}
