package world.entities;

class Entity extends zf.engine2.Entity {
	// ---- Aliases ---- //
	public var world(get, never): World;

	inline function get_world()
		return cast this.__world__;

	// ---- Components ---- //
	public var render(default, set): RenderComponent;

	public function set_render(component: RenderComponent): RenderComponent {
		final prev = this.render;
		this.render = component;
		onComponentChanged(prev, this.render);
		return this.render;
	}

	public var echo(default, set): EchoComponent;

	public function set_echo(c: EchoComponent): EchoComponent {
		final prev = this.echo;
		this.echo = c;
		onComponentChanged(prev, this.echo);
		return this.echo;
	}

	public var bulletWeapon(default, set): BulletWeaponComponent;

	public function set_bulletWeapon(component: BulletWeaponComponent): BulletWeaponComponent {
		final prev = this.bulletWeapon;
		this.bulletWeapon = component;
		onComponentChanged(prev, this.bulletWeapon);
		return this.bulletWeapon;
	}

	public var health(default, set): HealthComponent;

	public function set_health(component: HealthComponent): HealthComponent {
		final prev = this.health;
		this.health = component;
		onComponentChanged(prev, this.health);
		return this.health;
	}

	public var ai(default, set): AIComponent;

	public function set_ai(component: AIComponent): AIComponent {
		final prev = this.ai;
		this.ai = component;
		onComponentChanged(prev, this.ai);
		return this.ai;
	}

	public var buff(default, set): BuffComponent;

	public function set_buff(component: BuffComponent): BuffComponent {
		final prev = this.buff;
		this.buff = component;
		onComponentChanged(prev, this.buff);
		return this.buff;
	}

	// ---- Override x/y getter setter ---- //

	/**
		Since we are using echo, we don't want to store the x/y position
	**/
	override public function get_x(): Float {
		if (this.echo == null) return super.get_x();
		return this.echo.body.x;
	}

	override public function set_x(x: Float): Float {
		if (this.echo == null) return super.set_x(x);
		return this.echo.body.x = x;
	}

	override public function get_y(): Float {
		if (this.echo == null) return super.get_y();
		return this.echo.body.y;
	}

	override public function set_y(y: Float): Float {
		if (this.echo == null) return super.set_y(y);
		return this.echo.body.y = y;
	}

	override public function setPosition(x: Float, y: Float) {
		if (this.echo == null) return super.setPosition(x, y);
		this.echo.body.x = x;
		this.echo.body.y = y;
	}

	override public function get_rotation(): Float {
		if (this.echo == null) return super.get_rotation();
		return this.echo.body.rotation;
	}

	override public function set_rotation(r: Float): Float {
		if (this.echo == null) return super.set_rotation(r);
		return this.echo.body.rotation = r;
	}

	public var faction: Faction = Faction.FTNeutralPassive;

	// ---- Game Specific code ---- //
	public function new(id: Int = -1) {
		super(id);
	}

	override public function update(dt: Float) {
		if (this.render != null) this.render.sync();
	}
}
