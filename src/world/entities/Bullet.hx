package world.entities;

class Bullet extends Entity {
	public var velocity: Point2f;
	public var target: Point2f;
	public var radius: Float;
	public var damage: Int;
	public var pierce: Bool = false;

	public var color: Color = 0xffffffff;

	public var collided: Map<Int, Bool>;
	public var onHitBuff: Buff;

	public var splitOnCollide: Int = 0;
	public var splitDamageReduction: Float = 0.25;
	public var splitBitmap: Void->h2d.Bitmap;

	public var damageRadius: Float = 0;

	public var bm: h2d.Bitmap;

	public var moveAmount: Null<Float> = null;
	public var moveSpeed: Float = 0;

	public function new(id: Int, faction: Faction, velocity: Point2f, radius: Float = 2, target: Point2f = null,
			bm: h2d.Bitmap = null) {
		super(id);
		this.typeId = "bullet";
		this.faction = faction;
		this.velocity = velocity;
		this.radius = radius;
		this.target = target;

		this.collided = new Map<Int, Bool>();

		this.render = new RenderComponent();

		if (bm == null) bm = Assets.res.getBitmap("entity:bullet:small");
		this.bm = bm;
		final size = this.bm.getSize();
		this.bm.x = -size.width / 2;
		this.bm.y = -size.height / 2;
		this.render.object.addChild(bm);

		this.echo = new EchoComponent(new echo.Body({
			shape: {
				type: CIRCLE,
				radius: radius,
			}
		}), false);
	}
}
