package world.entities;

class Player extends Entity {
	public var belly(default, set): BellyComponent;

	public function set_belly(component: BellyComponent): BellyComponent {
		final prev = this.belly;
		this.belly = component;
		onComponentChanged(prev, this.belly);
		return this.belly;
	}

	public function new(id: Int) {
		super(id);
		this.typeId = "player";
		this.faction = FTPlayer;

		this.render = new RenderComponent();

		var shadow = Assets.res.getBitmap("shadow:small");
		shadow.x = -8;
		shadow.y = -8;
		this.render.main.addChild(shadow);

		final bm = Assets.res.getAnim("entity:player");
		bm.speed = 4;
		bm.x = -8;
		bm.y = -8;
		this.render.main.addChild(bm);
		this.render.flip = true;

		this.echo = new EchoComponent(new echo.Body({
			mass: 10000,
			material: {
				elasticity: 1.0,
			},
			shape: {
				type: CIRCLE,
				radius: 6,
			}
		}));

		this.bulletWeapon = new BulletWeaponComponent();
		this.bulletWeapon.damage = 400;
		this.bulletWeapon.baseAttackSpeed = 4;
		this.bulletWeapon.bulletSpeed = 250;

		this.health = new HealthComponent();
		this.belly = new BellyComponent();
		this.buff = new BuffComponent();
	}
}
