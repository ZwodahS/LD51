package world.rules;

class Buff {
	public var type: BuffType;
	public var duration: Float;

	public function new(bt: BuffType, duration: Float) {
		this.type = bt;
		this.duration = duration;
	}
}
