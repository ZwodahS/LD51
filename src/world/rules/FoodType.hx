package world.rules;

enum abstract FoodType(String) from String to String {
	public var Carrot = "food:carrot";
	public var Cabbage = "food:cabbage";
	public var Potato = "food:potato";
	public var Apple = "food:apple";
	public var Orange = "food:orange";
	public var Cherry = "food:cherry";
	public var Banana = "food:banana";
}
