package world.rules;

enum abstract BuffType(String) from String to String {
	public var Frozen = "buff:frozen";

	public static function getIcon(bt: BuffType): h2d.Bitmap {
		return switch (bt) {
			case Frozen: Assets.res.getBitmap("debuff:ice");
			default: null;
		}
	}
}
