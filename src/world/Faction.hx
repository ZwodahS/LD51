package world;

enum abstract Faction(String) from String to String {
	public var FTPlayer = "FPlayer";
	public var FTEnemy = "FEnemy";
	public var FTNeutralPassive = "FTNeutralPassive";
	public var FTNeutralHostile = "FTNeutralHostile";
	public var FTPowerup = "FTPowerup";
}
