package world.systems;

class SystemTemplate extends System {
	public function new() {
		super();
	}

	override public function init(world: zf.engine2.World) {
		super.init(world);
	}

	override public function onEvent(event: hxd.Event): Bool {
		return false;
	}

	/**
		reset the system to the same state after constructor
	**/
	override public function reset() {
		super.reset();
	}

	/**
		Dispose the system data
	**/
	override public function dispose() {
		super.dispose();
	}

	/**
		update loop
	**/
	override public function update(dt: Float) {
		super.update(dt);
	}

	override public function onEntityAdded(entity: zf.engine2.Entity) {
		super.onEntityAdded(entity);
		final e: Entity = cast entity;
	}

	override public function onEntityRemoved(entity: zf.engine2.Entity) {
		super.onEntityRemoved(entity);
		final e: Entity = cast entity;
	}
}
