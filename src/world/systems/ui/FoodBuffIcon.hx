package world.systems.ui;

import world.components.BellyComponent.FoodBuff;

class FoodBuffIcon extends h2d.Object {
	public var buff: FoodBuff;
	public var text: h2d.Text;

	public function new(buff: FoodBuff, tooltipHelper: zf.ui.TooltipHelper) {
		super();
		this.buff = buff;
		this.addChild(Assets.res.getBitmap(this.buff.type));
		this.text = cast Globals.uiBuilder.makeObjectFromStruct({
			type: "text",
			conf: {
				font: Assets.bodyFonts[0],
				text: '${buff.duration.round(1)}s',
				textColor: 0xfffffbe5,
			}
		});
		text.maxWidth = 16;
		text.textAlign = Center;
		text.y = 10;
		this.addChild(text);

		final string = switch (buff.type) {
			case Carrot: "Bullet pierce";
			case Cabbage: "Deals 25% damage as AOE. Higher duration = higher radius";
			case Potato: "Split into smaller bullet on impact with enemy.";
			case Apple: "Increase damage of bullet. Higher duration = higher damager";
			case Orange: "Increase rate of fire, Higher duration = higher rate";
			case Cherry: "Increase number of bullets";
			case Banana: "Increase movement speed, Higher duration = faster speed";
		}

		final window = new TooltipWindow(string);
		tooltipHelper.attachWindowTooltip(this, window);
	}

	public function update() {
		this.text.text = '${buff.duration.round(1)}s';
	}
}
