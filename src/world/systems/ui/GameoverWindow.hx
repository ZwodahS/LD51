package world.systems.ui;

class GameoverWindow extends h2d.Object {
	public var world: World;

	public var scoreText: h2d.Text;

	public function new(world: World) {
		super();

		this.world = world;

		final bg = Assets.fromColor(0xff1a1212, Globals.game.gameWidth + 4, Globals.game.gameHeight + 4);
		bg.setX(-2).setY(-2);
		bg.visible = true;
		bg.alpha = 0.4;
		this.addChild(bg);

		// block any interaction
		final interactive = new Interactive(Globals.game.gameWidth, Globals.game.gameHeight);
		this.addChild(interactive);
		interactive.propagateEvents = false;

		final flow = Globals.uiBuilder.makeObjectFromXMLString('<layout-vflow spacing="10"></layout-vflow>');

		this.scoreText = cast Globals.uiBuilder.makeObjectFromStruct({
			type: "text",
			conf: {
				font: Assets.bodyFonts[2],
				text: 'Score: 0',
				textColor: 0xfffffbe5,
			}
		});
		flow.addChild(scoreText);

		final startGameButton = zf.ui.GenericButton.fromTiles(Assets.res.getTiles("button:menu"));
		startGameButton.font = Assets.bodyFonts[2];
		startGameButton.text = "Restart";
		startGameButton.onLeftClick = () -> {
			final worldState = WorldState.newGame();
			this.world.worldState = worldState;
			this.world.startGame();
			this.remove();
		}
		flow.addChild(startGameButton);

		flow.setX(Globals.game.gameWidth, AlignCenter).setY(100);
		this.addChild(flow);
	}

	public function update() {
		this.scoreText.text = 'Score: ${this.world.worldState.score}';
	}
}
