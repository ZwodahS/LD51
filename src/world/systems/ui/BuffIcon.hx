package world.systems.ui;

class BuffIcon extends h2d.Object {
	public var buff: Buff;
	public var text: h2d.Text;

	public function new(buff: Buff, tooltipHelper: zf.ui.TooltipHelper) {
		super();
		this.buff = buff;
		this.addChild(this.buff.type.getIcon());
		this.text = cast Globals.uiBuilder.makeObjectFromStruct({
			type: "text",
			conf: {
				font: Assets.bodyFonts[0],
				text: '${buff.duration.round(1)}s',
				textColor: 0xfffffbe5,
			}
		});
		text.maxWidth = 16;
		text.textAlign = Center;
		text.y = 10;
		this.addChild(text);

		final string = switch (buff.type) {
			case Frozen: "Reduce movement speed by 80%";
		}

		final window = new TooltipWindow(string);
		tooltipHelper.attachWindowTooltip(this, window);
	}

	public function update() {
		this.text.text = '${buff.duration.round(1)}s';
	}
}
