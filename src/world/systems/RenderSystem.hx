package world.systems;

import world.systems.ui.BuffIcon;
import world.systems.ui.FoodBuffIcon;
import world.systems.ui.GameoverWindow;

enum abstract L(Int) from Int to Int {
	public var Floor = 0;
	public var Walls = 1;
	public var Powerup = 4;
	public var Enemies = 5;
	public var Player = 10;
	public var Particle = 30;
	public var Bullet = 50;
	public var Debug = 100;
}

class RenderSystem extends System {
	// ---- All the various layers to draw into ---- //

	/**
		The main draw layer
	**/
	public final drawLayers: h2d.Layers;

	/**
		Background layers.
	**/
	public final bgLayers: h2d.Layers;

	/**
		The main world layers
	**/
	public final worldLayers: h2d.Layers;

	/**
		For particle effects
	**/
	public final particleLayers: h2d.Layers;

	/**
		Debug layers.
	**/
	public final debugWorldLayers: h2d.Layers;

	/**
		For Hud
	**/
	public final hudLayer: h2d.Layers;

	/**
		Window layers, used by windowRenderSystem
	**/
	public final windowLayers: h2d.Layers;

	/**
		A non blocking animator.

		Use this for animation that is non-blocking
	**/
	public final animator: zf.up.Updater;

	/**
		WindowRenderSystem, a sub system for rendering windows
	**/
	public final windowRenderSystem: zf.ui.WindowRenderSystem;

	/**
		Tooltip Helper to attach tooltip to any h2d.Object.

		For game entity, it is better to use tooltip component.
	**/
	public final tooltipHelper: zf.ui.TooltipHelper;

	public function new() {
		super();
		this.animator = new zf.up.Updater();

		// ---- Setup all the various layers ---- //
		this.drawLayers = new h2d.Layers();
		this.drawLayers.add(this.bgLayers = new h2d.Layers(), 0);
		final displayArea = new h2d.Mask(Std.int(Constants.WorldSize.x), Std.int(Constants.WorldSize.y));
		this.worldLayers = new h2d.Layers(displayArea);
		this.drawLayers.add(displayArea, 50);
		displayArea.x = 24;
		displayArea.y = 40;
		this.worldLayers.add(this.particleLayers = new h2d.Layers(), L.Particle);
		this.worldLayers.add(this.debugWorldLayers = new h2d.Layers(), L.Debug);
		this.drawLayers.add(this.hudLayer = new h2d.Layers(), 50);
		this.drawLayers.add(this.windowLayers = new h2d.Layers(), 100);

		// ---- Setup WindowRenderSystem ---- //
		final windowBounds = new h2d.col.Bounds();
		windowBounds.xMin = 15;
		windowBounds.yMin = 15;
		windowBounds.xMax = Globals.game.gameWidth - 15;
		windowBounds.yMax = Globals.game.gameHeight - 15;
		this.windowRenderSystem = new zf.ui.WindowRenderSystem(windowBounds, this.windowLayers);
		this.windowRenderSystem.defaultRenderDirection = [Down, Right, Left, Up];
		this.windowRenderSystem.defaultSpacing = 5;

		this.tooltipHelper = new zf.ui.TooltipHelper(this.windowRenderSystem);

		this.bgLayers.addChild(Assets.fromColor(0xff352639, Globals.game.gameWidth, Globals.game.gameHeight));

		renderHud();
		renderBuff();
		renderButton();
	}

	override public function init(world: zf.engine2.World) {
		super.init(world);

		// @:listen EchoSystem MOnWorldStateSet 0
		dispatcher.listen(MOnWorldStateSet.MessageType, (message: zf.Message) -> {
			for (entity in this.world.worldState.entities) {
				onEntityAdded(entity);
			}
		}, 0);

		// @:listen RenderSystem MOnGameover 0
		dispatcher.listen(MOnGameover.MessageType, (message: zf.Message) -> {
			showGameoverMenu();
		}, 0);
	}

	override public function onEntityAdded(e: zf.engine2.Entity) {
		final entity: Entity = cast e;
		if (entity.render == null) return;
		entity.render.sync();
		final layer = switch (entity.typeId) {
			case "player": L.Player;
			case "food": L.Powerup;
			case "wall": L.Walls;
			case "bullet": L.Bullet;
			default: entity.faction == FTEnemy ? L.Enemies : L.Floor;
		}
		this.worldLayers.add(entity.render.object, layer);
	}

	override public function onEntityRemoved(e: zf.engine2.Entity) {
		final entity: Entity = cast e;
		if (entity.render == null) return;
		this.worldLayers.removeChild(entity.render.object);
	}

	override public function reset() {
		this.animator.clear();
		this.windowLayers.removeChildren();
		this.worldLayers.removeChildren();
		this.particleLayers.removeChildren();
		this.worldLayers.add(this.particleLayers, L.Debug);
		this.worldLayers.add(this.debugWorldLayers, L.Debug);
		renderWorldBg();
	}

	function renderWorldBg() {
		for (y in 0...18) {
			for (x in 0...37) {
				final bm = Assets.res.getBitmap("tile:grass", (x * 7 + y) % 5);
				bm.x = x * 16;
				bm.y = y * 16;
				this.worldLayers.add(bm, L.Floor);
			}
		}
	}

	override public function update(dt: Float) {
		super.update(dt);

		dt = dt * this.world.worldSpeed;
		this.animator.update(dt);
		this.worldLayers.ysort(L.Enemies);

		if (this.world.worldState.player != null) {
			updateHealth();
			updateBuff();
		}
	}

	function updateHealth() {
		final player = this.world.worldState.player;
		this.hungerBar.maxValue = player.health.max >= player.health.current ? player.health.max : player.health.current;
		this.hungerBar.value = player.health.current;
		this.hungerBar.textValue = '${(player.health.current/1000).round(1)}s';
	}

	// ---- Pop up for restarting the game ---- //
	var gameoverWindow: GameoverWindow;

	function showGameoverMenu() {
		if (this.gameoverWindow == null) {
			this.gameoverWindow = new GameoverWindow(this.world);
		}
		gameoverWindow.visible = true;
		gameoverWindow.update();
		this.windowLayers.addChild(gameoverWindow);
	}

	// ----  ---- //

	public function explode(color: Color, position: Point2f, split: Int = 8, explodeDuration = .2, deviation = 16.0) {
		final t = Assets.res.getBitmap('explodeTile');
		t.x = position.x - 8;
		t.y = position.y - 8;
		t.color.setColor(0xFF000000 | color);
		t.alpha = .5;
		this.particleLayers.addChild(t);
		final e = new zf.up.animations.Explode(t, split, explodeDuration, deviation);
		this.animator.run(e);
	}

	// ---- hud ---- //
	public var hungerBar: zf.ui.Bar;

	function renderHud() {
		this.hungerBar = new zf.ui.Bar(Normal, 0xffffc88b, Assets.bodyFonts[1], 0xff111012, 200, 16);
		this.hungerBar.maxValue = 10000;
		this.hungerBar.value = 10000;
		this.hungerBar.textValue = "10.0";
		final flow = Globals.uiBuilder.makeObjectFromXMLString('<layout-hflow spacing="5" />');
		final text = Globals.uiBuilder.makeObjectFromStruct({
			type: "text",
			conf: {
				font: Assets.bodyFonts[1],
				text: "Hunger",
				textColor: 0xfffffbe5,
			}
		});
		flow.addChild(text);
		flow.addChild(hungerBar);
		this.hudLayer.addChild(flow);
		this.hudLayer.x = 20;
		this.hudLayer.y = 10;
	}

	public var buffFlow: h2d.Object;
	public var foodBuffs: Array<FoodBuffIcon>;
	public var buffs: Array<BuffIcon>;

	function renderBuff() {
		this.foodBuffs = [];
		this.buffs = [];
		this.buffFlow = Globals.uiBuilder.makeObjectFromXMLString('<layout-hflow spacing="5" />');
		this.buffFlow.x = 250;
		this.hudLayer.addChild(buffFlow);
	}

	function updateBuff() {
		final player = this.world.worldState.player;
		if (this.foodBuffs.length != player.belly.foods.length || this.buffs.length != player.buff.buff.length) {
			// if the buffs is different we will need to clean and update
			buffFlow.removeChildren();
			buffs.clear();
			foodBuffs.clear();
			for (f in player.belly.foods) {
				final fb = new FoodBuffIcon(f, this.tooltipHelper);
				buffFlow.addChild(fb);
				foodBuffs.push(fb);
			}

			for (b in player.buff.buff) {
				final fb = new BuffIcon(b, this.tooltipHelper);
				buffFlow.addChild(fb);
				buffs.push(fb);
			}
			return;
		}

		for (b in this.buffs) b.update();
		for (b in this.foodBuffs) b.update();
	}

	function renderButton() {
		final button = Utils.getDiscordBtn("Discord");
		button.setX(Globals.game.gameWidth, AnchorRight, 100).setY(5);
		this.hudLayer.addChild(button);
	}
}
