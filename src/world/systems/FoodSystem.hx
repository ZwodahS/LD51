package world.systems;

class FoodSystem extends System {
	public var foodSpawnSpots: Array<Point2f>;

	public function new() {
		super();

		this.foodSpawnSpots = [];
		final xSize = 6;
		final ySize = 4;
		final xWidth = Constants.WorldSize.x / xSize;
		final yWidth = Constants.WorldSize.y / ySize;
		for (x in 1...xSize) {
			for (y in 1...ySize) {
				this.foodSpawnSpots.push([x * xWidth, y * yWidth]);
			}
		}
	}

	override public function init(world: zf.engine2.World) {
		super.init(world);

		// @:listen FoodSystem MOnFoodCollected 0
		dispatcher.listen(MOnFoodCollected.MessageType, (message: zf.Message) -> {
			spawnRandomFood();
		}, 0);
	}

	public function spawnRandomFood() {
		var numFood = 1;
		if (this.world.worldState.level == 1) { // first food collected, we will spawn 3 food instead
			numFood = 4;
		}

		final player = this.world.worldState.player;
		final foods = this.world.worldState.food;

		// for each food, we will try to spawn away from the player.
		// for this, it is quite simple, we will always spawn in one of the 8 spots around the map
		this.foodSpawnSpots.shuffle(this.world.r);

		function canSpawnFood(p: Point2f) {
			final distance = p.distance([player.x, player.y]);
			if (distance < 30) return false; // too close to player
			for (currFood in foods) {
				final distance = p.distance([currFood.x, currFood.y]);
				if (distance < 30) return false; // too close to existing food
			}
			return true;
		}

		var i = 0;
		function findSpawnSpot() {
			while (i < this.foodSpawnSpots.length) {
				final p = this.foodSpawnSpots[i++];
				if (canSpawnFood(p)) return p;
			}
			return this.world.r.randomChoice(this.foodSpawnSpots);
		}

		for (_ in 0...numFood) {
			final f = Food.randomFood(this.world.nextId, this.world.r);
			final p = findSpawnSpot();
			f.x = p.x + this.world.r.randomInt(20) - 10;
			f.y = p.y + this.world.r.randomInt(20) - 10;
			addFood(f);
			this.dispatcher.dispatch(new MOnFoodSpawn(f, this.world.worldState.level));
		}
		this.world.worldState.level += 1;
	}

	override public function update(dt: Float) {
		if (this.world.worldState.level == 0) {
			// if this is the first start of the game, we will spawn the first food
			final f = Food.randomFood(this.world.nextId, this.world.r);
			f.x = Constants.WorldSize.x / 4 * 3;
			f.y = 175;
			addFood(f);
			this.world.worldState.level += 1;
		}
		checkFoodCollection();
	}

	function checkFoodCollection() {
		final foods: Array<Food> = [];

		final player = this.world.worldState.player;
		// we will only test food against player
		for (f in this.world.worldState.food) {
			if (f.echo.body.shape.collides(player.echo.body.shape) != null) {
				foods.push(f);
			}
		}

		for (f in foods) gatherFood(player, f);
	}

	function gatherFood(player: Entity, f: Food) {
		removeFood(f);
		if (player.health.current < player.health.max) {
			player.health.current = Math.clampI(player.health.current + f.foodValue, 0, player.health.max);
		}
		this.dispatcher.dispatch(new MOnFoodCollected(f));
	}

	function addFood(e: Food) {
		this.world.worldState.addEntity(e);
		this.world.registerEntity(e);
		this.world.worldState.food.push(e);
	}

	function removeFood(e: Food) {
		this.world.worldState.removeEntity(e);
		this.world.unregisterEntity(e);
		this.world.worldState.food.remove(e);
	}
}
