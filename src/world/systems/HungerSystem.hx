package world.systems;

class HungerSystem extends System {
	public function new() {
		super();
	}

	override public function init(world: zf.engine2.World) {
		super.init(world);
	}

	override public function onEvent(event: hxd.Event): Bool {
		return false;
	}

	/**
		reset the system to the same state after constructor
	**/
	override public function reset() {
		super.reset();
	}

	/**
		Dispose the system data
	**/
	override public function dispose() {
		super.dispose();
	}

	/**
		update loop
	**/
	override public function update(dt: Float) {
		super.update(dt);
		if (this.world.worldState == null) return;
		dt = dt * this.world.worldSpeed;

		final player = this.world.worldState.player;
		if (player == null) return;
		if (player.health.current <= 0) return;
		if (this.world.worldState.level == 1) return;

		// reduce the player hunter
		// since dt is in second, and therefore we will dt * 1000
		final amount = Std.int(dt * 1000);
		player.health.current -= amount;
		if (player.health.current <= 0) {
			player.health.current = 0;
			this.dispatcher.dispatch(new MOnHealthDepleted(player));
		}
	}
}
