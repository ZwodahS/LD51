package world.systems;

class AISystem extends System {
	public function new() {
		super();
	}

	override public function init(world: zf.engine2.World) {
		super.init(world);
	}

	override public function update(dt: Float) {
		super.update(dt);
		if (this.world.worldState == null || this.world.worldSpeed == 0) return;
		dt = dt * this.world.worldSpeed;

		for (e in this.world.worldState.entities) {
			if (e.ai == null) continue;
			e.ai.doNext(this.world, dt);
		}
	}
}
