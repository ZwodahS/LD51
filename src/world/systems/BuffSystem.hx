package world.systems;

class BuffSystem extends System {
	public function new() {
		super();
	}

	override public function init(world: zf.engine2.World) {
		super.init(world);
	}

	/**
		update loop
	**/
	override public function update(dt: Float) {
		super.update(dt);
		dt = dt * this.world.worldSpeed;

		final player = this.world.worldState.player;
		if (player == null) return;

		var toRemove = [];
		for (b in player.buff.buff) {
			b.duration -= dt;
			if (b.duration <= 0) toRemove.push(b);
		}
		for (b in toRemove) player.buff.buff.remove(b);
	}
}
