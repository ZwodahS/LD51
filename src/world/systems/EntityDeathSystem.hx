package world.systems;

class EntityDeathSystem extends System {
	public function new() {
		super();
	}

	override public function init(world: zf.engine2.World) {
		super.init(world);

		// @:listen EntityDeathSystem MOnHealthDepleted 0
		dispatcher.listen(MOnHealthDepleted.MessageType, (message: zf.Message) -> {
			final m: MOnHealthDepleted = cast message;
			onHealthDepleted(m);
		}, 0);
	}

	function onHealthDepleted(m: MOnHealthDepleted) {
		if (m.entity == this.world.worldState.player) {
			this.world.gameover();
			return;
		}

		// @todo animate death
		this.world.worldState.removeEntity(m.entity);
		this.world.unregisterEntity(m.entity);
		m.entity.dispose();
	}
}
