package world.systems;

class BellySystem extends System {
	public function new() {
		super();
	}

	override public function init(world: zf.engine2.World) {
		super.init(world);

		// @:listen BellySystem MOnFoodCollected 0
		dispatcher.listen(MOnFoodCollected.MessageType, (message: zf.Message) -> {
			final m: MOnFoodCollected = cast message;
			final player = this.world.worldState.player;
			if (player == null) return;

			player.belly.addFood(m.food.foodType, 11);
		}, 0);
	}

	override public function update(dt: Float) {
		super.update(dt);
		dt = dt * this.world.worldSpeed;

		final player = this.world.worldState.player;
		if (player == null) return;

		var toRemove = [];
		for (f in player.belly.foods) {
			f.duration -= dt;
			if (f.duration <= 0) toRemove.push(f);
		}
		for (f in toRemove) player.belly.foods.remove(f);
	}
}
