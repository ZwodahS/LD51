package world.systems;

class WeaponSystem extends System {
	public function new() {
		super();
	}

	override public function init(world: zf.engine2.World) {
		super.init(world);
	}

	override public function onEvent(event: hxd.Event): Bool {
		return false;
	}

	/**
		reset the system to the same state after constructor
	**/
	override public function reset() {
		super.reset();
	}

	/**
		Dispose the system data
	**/
	override public function dispose() {
		super.dispose();
	}

	/**
		update loop
	**/
	override public function update(dt: Float) {
		super.update(dt);
		dt = dt * this.world.worldSpeed;

		for (e in this.world.worldState.entities) {
			if (e.bulletWeapon != null) e.bulletWeapon.cooldown -= dt;
		}
	}
}
