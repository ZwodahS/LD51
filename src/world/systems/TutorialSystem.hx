package world.systems;

class TutorialSystem extends System {
	public function new() {
		super();
	}

	override public function init(world: zf.engine2.World) {
		super.init(world);

		// @:listen EchoSystem MOnWorldStateSet 0
		dispatcher.listen(MOnWorldStateSet.MessageType, (message: zf.Message) -> {
			if (this.world.worldState == null) return this.removeTutorial();
			if (this.world.worldState.level == 0) return this.addTutorial();
		}, 0);

		// @:listen FoodSystem MOnFoodCollected 0
		dispatcher.listen(MOnFoodCollected.MessageType, (message: zf.Message) -> {
			if (this.world.worldState.level != 0) removeTutorial();
		}, 0);
	}

	var tutorialObjects: Array<h2d.Object>;

	function removeTutorial() {
		if (this.tutorialObjects == null) return;

		for (t in this.tutorialObjects) {
			t.remove();
		}
		this.tutorialObjects = null;
	}

	function addTutorial() {
		if (this.tutorialObjects == null) {
			this.tutorialObjects = [];
			this.tutorialObjects.push(Assets.res.getBitmap("instruction:movement"));
			this.tutorialObjects.push(Assets.res.getBitmap("instruction:eat"));
			this.tutorialObjects.push(Assets.res.getBitmap("instruction:title"));
			tutorialObjects[0].x = 128;
			tutorialObjects[0].y = 120;
			tutorialObjects[1].x = 424;
			tutorialObjects[1].y = 130;
			tutorialObjects[2].setX(Constants.WorldSize.x, AlignCenter).setY(50);
		}
		this.world.renderSystem.worldLayers.add(tutorialObjects[0], 2);
		this.world.renderSystem.worldLayers.add(tutorialObjects[1], 2);
		this.world.renderSystem.worldLayers.add(tutorialObjects[2], 2);
	}
}
