package world.systems;

class TrackSystem extends System {
	public function new() {
		super();
	}

	override public function init(world: zf.engine2.World) {
		super.init(world);

		// @:listen TrackSystem MOnFoodCollected 0
		dispatcher.listen(MOnFoodCollected.MessageType, (message: zf.Message) -> {
			this.world.worldState.score += 1;
		}, 0);
	}
}
