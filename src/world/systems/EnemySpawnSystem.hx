package world.systems;

class EnemySpawnSystem extends System {
	public function new() {
		super();
	}

	override public function init(world: zf.engine2.World) {
		super.init(world);

		// @:listen EnemySpawnSystem MOnFoodSpawn 0
		dispatcher.listen(MOnFoodSpawn.MessageType, (message: zf.Message) -> {
			final m: MOnFoodSpawn = cast message;
			spawnEnemy(m.food.x, m.food.y, m.level);
		}, 0);
	}

	override public function onEvent(event: hxd.Event): Bool {
		return false;
	}

	function addEnemy(e: Entity) {
		this.world.worldState.addEntity(e);
		this.world.registerEntity(e);
	}

	function spawnEnemy(x: Float, y: Float, level: Int) {
		// default to spawning 2 enemies
		final entities: Array<Entity> = [];

		if (level <= 2) {
			entities.push(Enemy.makeFarmer(this.world.nextId));
		} else if (level <= 5) {
			entities.push(Enemy.makeFarmer(this.world.nextId));
			entities.push(Enemy.makeFarmer(this.world.nextId));
		} else if (level < 10) {
			entities.push(Enemy.makeFarmer(this.world.nextId));
			entities.push(Enemy.makeFarmer(this.world.nextId));
			if (level % 2 == 0) {
				entities.push(Enemy.makeRedMage(this.world.nextId));
			} else {
				entities.push(Enemy.makeBlueMage(this.world.nextId));
			}
		} else if (level == 10) {
			entities.push(Enemy.makeFarmer(this.world.nextId));
			entities.push(Enemy.makeFarmer(this.world.nextId));
			entities.push(Enemy.makeFarmer(this.world.nextId));
			entities.push(Enemy.makeGuard(this.world.nextId));
		} else if (level < 15) {
			entities.push(Enemy.makeFarmer(this.world.nextId));
			entities.push(Enemy.makeFarmer(this.world.nextId));
			entities.push(Enemy.makeFarmer(this.world.nextId));
			entities.push(Enemy.makeArcher(this.world.nextId));
			entities.push(Enemy.makeArcher(this.world.nextId));
		} else if (level == 15) {
			entities.push(Enemy.makeGuard(this.world.nextId));
			entities.push(Enemy.makeGuard(this.world.nextId));
			entities.push(Enemy.makeGuard(this.world.nextId));
			entities.push(Enemy.makeArcher(this.world.nextId));
			entities.push(Enemy.makeArcher(this.world.nextId));
		} else if (level < 20) {
			entities.push(Enemy.makeGuard(this.world.nextId));
			entities.push(Enemy.makeArcher(this.world.nextId));
			entities.push(Enemy.makeArcher(this.world.nextId));
			if (level % 2 == 0) {
				entities.push(Enemy.makeRedMage(this.world.nextId));
			} else {
				entities.push(Enemy.makeBlueMage(this.world.nextId));
			}
		} else {
			entities.push(Enemy.makeFarmer(this.world.nextId));
			entities.push(Enemy.makeFarmer(this.world.nextId));
			entities.push(Enemy.makeGuard(this.world.nextId));
			entities.push(Enemy.makeArcher(this.world.nextId));
			for (_ in 0...(Std.int((level - 20) / 10) + 1)) {
				switch (this.world.r.randomInt(2)) {
					case 0:
						entities.push(Enemy.makeRedMage(this.world.nextId));
					case 1:
						entities.push(Enemy.makeBlueMage(this.world.nextId));
					default:
				}
			}
		}

		for (e in entities) {
			e.health.max = Std.int(e.health.max * (1 + (0.1 * (level - 1))));
			e.health.current = e.health.max;
			e.bulletWeapon.damage = Std.int(e.bulletWeapon.damage * (1 + (0.03 * (level - 1))));
			e.x = x + this.world.r.randomInt(40) - 20;
			e.y = y + this.world.r.randomInt(40) - 20;
			this.addEnemy(e);
		}
	}
}
