package world.systems;

class CollisionSystem extends System {
	public function new() {
		super();
	}

	override public function init(world: zf.engine2.World) {
		super.init(world);

		// @:listen CollisionSystem MOnBulletCollide 0
		dispatcher.listen(MOnBulletCollide.MessageType, (message: zf.Message) -> {
			final m: MOnBulletCollide = cast message;
			onBulletCollide(m);
		}, 0);
	}

	function onBulletCollide(m: MOnBulletCollide) {
		// already collided
		if (m.bullet.pierce == true && m.hit.typeId != "wall") m.remove = false;

		if (m.bullet.collided[m.hit.id] == true) return;
		m.bullet.collided[m.hit.id] = true;

		var isPlayer = false;
		switch (m.bullet.faction) {
			case FTPlayer:
				isPlayer = true;
			case FTEnemy:
			case FTNeutralPassive:
			case FTNeutralHostile:
			default:
		}

		var splashed: Array<Entity> = [];

		if (m.bullet.damageRadius != 0) {
			var bulletBound = echo.Shape.circle(m.bullet.x, m.bullet.y, m.bullet.damageRadius, 1, 1);

			final factions: Array<Faction> = switch (m.bullet.faction) {
				case FTPlayer: [FTEnemy, FTNeutralPassive, FTNeutralHostile];
				case FTEnemy: [FTPlayer, FTNeutralPassive, FTNeutralHostile];
				case FTNeutralPassive: [FTPlayer, FTEnemy];
				case FTNeutralHostile: [FTPlayer, FTEnemy];
				default: [];
			}

			for (f in factions) {
				for (e in this.world.worldState.entitiesByFaction[f]) {
					if (e.echo == null || e.health == null || e == m.hit) continue;
					final c = bulletBound.collides(e.echo.body.shape);
					if (c != null) splashed.push(e);
				}
			}

			bulletBound.put();
		}

		if (m.bullet.splitOnCollide != 0 && m.hit.typeId != "wall") {
			// we create bullets and move it to the opposite direction

			final velocity = m.bullet.velocity.copy();
			velocity.x = -velocity.x;
			velocity.y = -velocity.y;
			final newBullets: Array<Bullet> = [];
			// @formatter:off
			final getBm = m.bullet.splitBitmap != null ? m.bullet.splitBitmap : () -> { return Assets.res.getBitmap("entity:bullet:smaller"); }

			newBullets.push(new Bullet(this.world.nextId, m.bullet.faction, velocity.copy(), m.bullet.radius/2, getBm()));

			if (m.bullet.splitOnCollide >= 1) {
				newBullets.push(new Bullet(this.world.nextId, m.bullet.faction, velocity.copy(), m.bullet.radius/2, getBm()));
				newBullets.push(new Bullet(this.world.nextId, m.bullet.faction, velocity.copy(), m.bullet.radius/2, getBm()));
				newBullets[1].velocity.rad = newBullets[1].velocity.rad - (0.125 * Math.PI);
				newBullets[2].velocity.rad = newBullets[2].velocity.rad + (0.125 * Math.PI);
			}

			if (m.bullet.splitOnCollide >= 2) {
				newBullets.push(new Bullet(this.world.nextId, m.bullet.faction, velocity.copy(), m.bullet.radius/2, getBm()));
				newBullets.push(new Bullet(this.world.nextId, m.bullet.faction, velocity.copy(), m.bullet.radius/2, getBm()));
				newBullets[3].velocity.rad = newBullets[3].velocity.rad - (0.25 * Math.PI);
				newBullets[4].velocity.rad = newBullets[4].velocity.rad + (0.25 * Math.PI);
			}

			for (bullet in newBullets) {
				bullet.color = m.bullet.color;
				bullet.collided[m.hit.id] = true;
				bullet.x = m.bullet.x;
				bullet.y = m.bullet.y;
				bullet.damage = Std.int(m.bullet.damage * 0.25);
				this.world.bulletSystem.addBullet(bullet);
			}
		}

		if (m.hit.health != null) {
			m.hit.health.current -= m.bullet.damage;
			if (m.hit.health.current <= 0) {
				this.dispatcher.dispatch(new MOnHealthDepleted(m.hit));
			}
		}

		if (m.bullet.onHitBuff != null && m.hit.buff != null) {
			m.hit.buff.addBuff(m.bullet.onHitBuff.type, m.bullet.onHitBuff.duration);
		}

		final splashDamage = Std.int(m.bullet.damage * 0.25);
		for (e in splashed) {
			e.health.current -= splashDamage;
			if (e.health.current <= 0) {
				this.dispatcher.dispatch(new MOnHealthDepleted(e));
			}
		}

		if (isPlayer == true) {
			final color = m.bullet.color;
			this.world.renderSystem.explode(color, [m.bullet.x, m.bullet.y]);
		}
	}
}
