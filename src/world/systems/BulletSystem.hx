package world.systems;

class BulletSystem extends System {
	var tempPositions: Array<Point2f> = [];

	public function new() {
		super();
		this.tempPositions = [];
		for (_ in 0...5) {
			this.tempPositions.push([0, 0]);
		}
	}

	override public function init(world: zf.engine2.World) {
		super.init(world);

		// @:listen BulletSystem MDoFireBullet 0
		dispatcher.listen(MDoFireBullet.MessageType, (message: zf.Message) -> {
			final m: MDoFireBullet = cast message;
			fire(m.entity, tempPositions[0].xy(m.x, m.y));
		}, 0);
	}

	override public function onEvent(event: hxd.Event): Bool {
		return false;
	}

	/**
		reset the system to the same state after constructor
	**/
	override public function reset() {
		super.reset();
	}

	/**
		Dispose the system data
	**/
	override public function dispose() {
		super.dispose();
	}

	/**
		update loop
	**/
	override public function update(dt: Float) {
		super.update(dt);
		if (this.world.worldState == null) return;

		dt = dt * this.world.worldSpeed;

		inline function testCollision(bullet: Bullet) {
			var hit: Entity = null;
			var bulletBound = bullet.echo.body.shape;
			var collision: echo.data.Data.CollisionData = null;

			final factions: Array<Faction> = switch (bullet.faction) {
				case FTPlayer: [FTEnemy, FTNeutralPassive, FTNeutralHostile];
				case FTEnemy: [FTPlayer, FTNeutralPassive, FTNeutralHostile];
				case FTNeutralPassive: [FTPlayer, FTEnemy];
				case FTNeutralHostile: [FTPlayer, FTEnemy];
				default: [];
			}

			for (f in factions) {
				for (e in this.world.worldState.entitiesByFaction[f]) {
					if (e.echo == null) continue;
					final c = bulletBound.collides(e.echo.body.shape);
					if (c != null) {
						hit = e;
						collision = c;
						break;
					}
				}
				if (hit != null) break;
			}

			if (hit != null) {
				final m = this.world.dispatcher.dispatch(new MOnBulletCollide(bullet, hit, collision));
				if (m.remove == true) removeBullet(bullet);
				collision.put();
				collision = null;
			}
		}

		for (bullet in this.world.worldState.bullets) {
			bullet.x += dt * bullet.velocity.x;
			bullet.y += dt * bullet.velocity.y;
			if (bullet.moveAmount != null) {
				bullet.moveAmount -= dt * bullet.moveSpeed;
				if (bullet.moveAmount <= 0) {
					removeBullet(bullet);
					continue;
				}
			}
			testCollision(bullet);
		}
	}

	public function fire(entity: Entity, t: Point2f) {
		if (entity.bulletWeapon == null) return;
		if (entity.bulletWeapon.isReady == false) return;

		// handle player's attack differently
		if (entity == this.world.worldState.player) return handlePlayerAttack(entity, t);

		entity.bulletWeapon.cooldown = 1 / entity.bulletWeapon.baseAttackSpeed;

		final bullet = entity.bulletWeapon.makeBullet(this.world.nextId, t);
		bullet.faction = entity.faction;

		final s = tempPositions[1].xy(entity.x, entity.y);
		bullet.velocity = getVector(s, t, entity.bulletWeapon.bulletSpeed).copy();
		bullet.x = s.x;
		bullet.y = s.y;
		bullet.render.object.rotation = bullet.velocity.rad;
		bullet.moveSpeed = entity.bulletWeapon.bulletSpeed;
		if (entity.bulletWeapon.maxRange != null) bullet.moveAmount = entity.bulletWeapon.maxRange;

		addBullet(bullet);
	}

	function getVector(s: Point2f, t: Point2f, speed: Float) {
		final velocity = tempPositions[2].xy(t.x - s.x, t.y - s.y).normalize().scale(speed);
		return velocity;
	}

	function handlePlayerAttack(entity: Entity, t: Point2f) {
		final player: Player = cast entity;
		// get the belly content
		// can do this via message but that might be overcomplicating things

		var appleDuration: Float = 0;
		var hasCherry: Bool = false;
		var hasCarrot: Bool = false;
		var orangeDuration: Float = 0;
		var hasPotato: Bool = false;
		var cabbageDuration: Float = 0;
		for (ft in player.belly.foods) {
			switch (ft.type) {
				case Apple:
					appleDuration = ft.duration;
				case Cherry:
					hasCherry = true;
				case Carrot:
					hasCarrot = true;
				case Orange:
					orangeDuration = ft.duration;
				case Potato:
					hasPotato = true;
				case Cabbage:
					cabbageDuration = ft.duration;
				default:
			}
		}

		inline function makeBullet(t: Point2f) {
			final bullet = new Bullet(this.world.nextId, entity.faction, null, entity.bulletWeapon.radius, null,
				Assets.res.getBitmap("entity:bullet:slime"));
			final s = tempPositions[1].xy(entity.x, entity.y);
			bullet.velocity = getVector(s, t, entity.bulletWeapon.bulletSpeed).copy();
			bullet.damage = entity.bulletWeapon.damage;
			bullet.x = s.x;
			bullet.y = s.y;

			bullet.color = 0xff74c2d1;

			return bullet;
		}

		final bullets: Array<Bullet> = [];
		bullets.push(makeBullet(t));
		// if cherry, we add 2 more attack vector that is 45 degree
		if (hasCherry == true) {
			bullets.push(makeBullet(t));
			bullets[1].velocity.rad = bullets[1].velocity.rad - (0.125 * Math.PI);
			bullets.push(makeBullet(t));
			bullets[2].velocity.rad = bullets[2].velocity.rad + (0.125 * Math.PI);
		}

		entity.bulletWeapon.cooldown = 1 / entity.bulletWeapon.baseAttackSpeed;
		if (orangeDuration > 0) entity.bulletWeapon.cooldown /= (1 + (orangeDuration / 15));

		// modify the bullet based on other effects
		for (bullet in bullets) {
			if (appleDuration > 0) bullet.damage = Std.int(bullet.damage * (1.1 + (appleDuration / 5)));
			if (hasCarrot) bullet.pierce = true;
			if (hasPotato) {
				bullet.splitOnCollide = 2;
				bullet.splitBitmap = () -> {
					return Assets.res.getBitmap("entity:bullet:slimesmall");
				}
			}
			if (cabbageDuration > 0) bullet.damageRadius = 10 + cabbageDuration;
		}

		for (bullet in bullets) addBullet(bullet);
	}

	public function addBullet(bullet: Bullet) {
		this.world.worldState.addEntity(bullet);

		this.world.registerEntity(bullet);
	}

	public function removeBullet(bullet: Bullet) {
		this.world.worldState.removeEntity(bullet);
		this.world.unregisterEntity(bullet);
		bullet.dispose();
	}
}
