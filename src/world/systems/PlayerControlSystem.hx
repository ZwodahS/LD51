package world.systems;

class PlayerControlSystem extends System {
	public function new() {
		super();
	}

	override public function update(dt: Float) {
		super.update(dt);
		if (this.world.worldState == null) return;
		if (this.world.worldState.player == null) return;

		final player: Player = this.world.worldState.player;

		// handle movement
		var xVelocity: Float = 0;
		var yVelocity: Float = 0;
		var moveSpeed: Float = 80;

		final duration = player.belly.getDuration(FoodType.Banana);
		if (duration > 0) moveSpeed *= (1.2 + (duration / 30));
		if (player.buff.has(BuffType.Frozen) == true) moveSpeed *= 0.3;

		if (hxd.Key.isDown(hxd.Key.A)) xVelocity -= 1;
		if (hxd.Key.isDown(hxd.Key.D)) xVelocity += 1;
		if (hxd.Key.isDown(hxd.Key.W)) yVelocity -= 1;
		if (hxd.Key.isDown(hxd.Key.S)) yVelocity += 1;

		if (xVelocity != 0 && yVelocity != 0) {
			xVelocity *= 0.9;
			yVelocity *= 0.9;
		}

		player.echo.body.velocity = [xVelocity * moveSpeed, yVelocity * moveSpeed];

		// check for firing
		if (player.bulletWeapon.isReady == true
			&& (hxd.Key.isDown(hxd.Key.MOUSE_LEFT) || hxd.Key.isDown(hxd.Key.SPACE))) {
			final scene = this.world.renderSystem.drawLayers.getScene();
			final positionX = scene.mouseX;
			final positionY = scene.mouseY;
			final pt = this.world.renderSystem.worldLayers.globalToLocal(new h2d.col.Point(positionX, positionY));
			this.dispatcher.dispatch(new MDoFireBullet(player, pt.x, pt.y));
		}
	}
}
