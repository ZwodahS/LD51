package world.components;

/**
	Handle player's belly
**/
class FoodBuff {
	public var type: FoodType;
	public var duration: Float;

	public function new(ft: FoodType, duration: Float) {
		this.type = ft;
		this.duration = duration;
	}
}

class BellyComponent extends Component {
	public static final ComponentType = "BellyComponent";

	public var foods: Array<FoodBuff>;

	public function new() {
		this.foods = [];
	}

	public function addFood(ft: FoodType, duration: Float) {
		for (f in this.foods) {
			if (f.type != ft) continue;
			f.duration += duration;
			f.duration = Math.clampF(f.duration, 0, 60);
			return;
		}
		this.foods.push(new FoodBuff(ft, duration));
	}

	public function has(ft: FoodType): Bool {
		for (f in this.foods) {
			if (f.type == ft) return true;
		}
		return false;
	}

	public function getDuration(ft: FoodType): Float {
		for (f in this.foods) {
			if (f.type == ft) return f.duration;
		}
		return 0;
	}
}
