package world.components;

class BuffComponent extends Component {
	public static final ComponentType = "BuffComponent";

	public var buff: Array<Buff>;

	public function new() {
		this.buff = [];
	}

	public function addBuff(bt: BuffType, duration: Float) {
		for (b in this.buff) {
			if (b.type != bt) continue;
			b.duration += duration;
			return;
		}
		this.buff.push(new Buff(bt, duration));
	}

	public function has(bt: BuffType): Bool {
		for (b in this.buff) {
			if (b.type == bt) return true;
		}
		return false;
	}

	public function getDuration(bt: BuffType): Float {
		for (b in this.buff) {
			if (b.type == bt) return b.duration;
		}
		return 0;
	}
}
