package world.components;

class EchoComponent extends Component {
	public static final ComponentType = "EchoComponent";

	public var body: echo.Body;

	public var addToSimulation: Bool = true;

	public function new(body: echo.Body, addToSimulation: Bool = true) {
		this.body = body;
		this.addToSimulation = addToSimulation;
	}
}
