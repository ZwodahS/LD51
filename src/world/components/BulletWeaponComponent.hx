package world.components;

class BulletWeaponComponent extends Component {
	public static final ComponentType = "BulletWeaponComponent";

	public var baseAttackSpeed: Float = 1; // number of attack per second

	public var cooldown: Float = 1; // the current cooldown of the weapon

	public var damage: Int = 500;

	public var bulletSpeed: Float = 100;

	public var isReady(get, never): Bool;

	public var bmId: String = "entity:bullet:small";
	public var radius: Int = 2;

	public var attackRange: Null<Float> = null;
	public var maxRange: Null<Float> = null;

	inline function get_isReady()
		return this.cooldown <= 0;

	public function new() {}

	dynamic public function makeBullet(id: Int, target: Point2f): Bullet {
		final bullet = new Bullet(id, FTNeutralPassive, null, this.radius, null, Assets.res.getBitmap(this.bmId));
		bullet.damage = entity.bulletWeapon.damage;
		return bullet;
	}
}
