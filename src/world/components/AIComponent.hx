package world.components;

class AIComponent extends Component {
	public static final ComponentType = "AIComponent";

	public function new() {}

	dynamic public function doNext(w: World, dt: Float) {}
}
