package world.components;

class HealthComponent extends Component {
	public static final ComponentType = "HealthComponent";

	// we use int instead of float, since this is easier
	// this is essentially 10 seconds, or 10k ms
	public var max: Int = 10000;
	public var current(default, set): Int = 10000;

	public function set_current(c: Int): Int {
		return this.current = Math.clampI(c, 0, null);
	}

	public function new() {}
}
