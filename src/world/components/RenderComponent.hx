package world.components;

/**
	The main render component for all entities
**/
class RenderComponent extends Component {
	public var object: h2d.Object;

	public var main: h2d.Object;
	public var hud: h2d.Object;

	public var syncRotation: Bool = false;
	public var flip: Bool = false;

	public function new(object: h2d.Object = null) {
		this.object = object == null ? new h2d.Object() : object;
		this.object.addChild(this.main = new h2d.Object());
		this.object.addChild(this.hud = new h2d.Object());
	}

	inline public function sync() {
		if (this.entity.x != this.object.x) this.object.x = this.entity.x;
		if (this.entity.y != this.object.y) this.object.y = this.entity.y;
		if (this.syncRotation == true && this.entity.rotation != this.object.rotation) {
			this.main.rotation = this.main.rotation;
		}

		if (this.flip && this.entity.echo != null) {
			final scaleX = this.entity.echo.body.velocity.x > 0 ? 1 : -1;
			if (this.main.scaleX != scaleX) this.main.scaleX = scaleX;
		}
	}

	override public function dispose() {
		this.object.remove();
		this.object = null;
	}
}
