package world.messages;

class MOnFoodCollected extends zf.Message {
	public static final MessageType = "MOnFoodCollected";

	public var food: Food;

	public function new(food: Food) {
		super(MessageType);
		this.food = food;
	}
}
