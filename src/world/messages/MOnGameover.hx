package world.messages;

class MOnGameover extends zf.Message {
	public static final MessageType = "MOnGameover";

	public function new() {
		super(MessageType);
	}
}
