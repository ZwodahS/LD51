package world.messages;

class MDoFireBullet extends zf.Message {
	public static final MessageType = "MDoFireBullet";

	public var entity: Entity;
	public var x: Float;
	public var y: Float;

	public function new(entity: Entity, x: Float, y: Float) {
		super(MessageType);
		this.entity = entity;
		this.x = x;
		this.y = y;
	}
}
