package world.messages;

class MOnBulletCollide extends zf.Message {
	public static final MessageType = "MOnBulletCollide";

	public var bullet: Bullet;
	public var hit: Entity;
	public var data: echo.data.Data.CollisionData;

	public var remove: Bool = true;

	public function new(bullet: Bullet, hit: Entity, data: echo.data.Data.CollisionData) {
		super(MessageType);
		this.bullet = bullet;
		this.hit = hit;
		this.data = data;
	}
}
