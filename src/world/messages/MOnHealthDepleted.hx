package world.messages;

class MOnHealthDepleted extends zf.Message {
	public static final MessageType = "MOnHealthDepleted";

	public var entity: Entity;

	public function new(e: Entity) {
		super(MessageType);
		this.entity = e;
	}
}
