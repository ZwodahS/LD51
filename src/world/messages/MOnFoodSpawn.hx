package world.messages;

class MOnFoodSpawn extends zf.Message {
	public static final MessageType = "MOnFoodSpawn";

	public var food: Food;
	public var level: Int;

	public function new(f: Food, level: Int) {
		super(MessageType);
		this.food = f;
		this.level = level;
	}
}
