package world;

class World extends zf.engine2.World {
	/**
		Store the main rule object
	**/
	public var rules: Rules;

	// ---- Systems ---- //

	/**
		Main rendering system
	**/
	public var renderSystem: RenderSystem;

	public var echoSystem: EchoSystem;

	public var bulletSystem: BulletSystem;

	/**
		World State
	**/
	public var worldState(default, set): WorldState = null;

	public function set_worldState(s: WorldState): WorldState {
		this.worldState = s;
		this.reset();
		this.dispatcher.dispatch(new MOnWorldStateSet());
		for (e in this.worldState.entities) {
			this.registerEntity(e);
		}
		return this.worldState;
	}

	public var nextId(get, never): Int;

	inline function get_nextId(): Int {
		return this.worldState == null ? 0 : this.worldState.nextId;
	}

	/**
		User Profile
	**/
	public var profile: Profile;

	public var worldSpeed: Float = 1.0;

	public function new(rules: Rules, profile: Profile) {
		super();
		this.rules = rules;
		this.profile = profile;

		this.addSystem(this.renderSystem = new RenderSystem());
		this.addSystem(new AISystem());
		this.addSystem(new PlayerControlSystem());
		this.addSystem(this.echoSystem = new EchoSystem());
		this.addSystem(new EnemySpawnSystem());
		this.addSystem(this.bulletSystem = new BulletSystem());
		this.addSystem(new WeaponSystem());
		this.addSystem(new HungerSystem());
		this.addSystem(new CollisionSystem());
		this.addSystem(new EntityDeathSystem());
		this.addSystem(new FoodSystem());
		this.addSystem(new BuffSystem());
		this.addSystem(new BellySystem());
		this.addSystem(new TutorialSystem());
		this.addSystem(new TrackSystem());
	}

	public function startGame() {
		this.worldSpeed = 1;
	}

	public function gameover() {
		this.worldSpeed = 0;
		this.dispatcher.dispatch(new MOnGameover());
	}
}
