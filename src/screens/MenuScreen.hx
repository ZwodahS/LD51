package screens;

import world.WorldState;

class MenuScreen extends zf.Screen {
	public function new() {
		super();

		final flow = Globals.uiBuilder.makeObjectFromXMLString('<layout-vflow spacing="10"></layout-vflow>');

		final startGameButton = zf.ui.GenericButton.fromTiles(Assets.res.getTiles("button:menu"));
		startGameButton.font = Assets.bodyFonts[2];
		startGameButton.text = "Start";
		startGameButton.onLeftClick = () -> {
			final world = new World(Globals.rules, Globals.currentProfile);
			world.worldState = WorldState.newGame();
			world.startGame();
			this.game.switchScreen(new GameScreen(world));
		}
		flow.addChild(startGameButton);

		flow.setX(Globals.game.gameWidth, AlignCenter).setY(200);
		this.addChild(flow);
	}

	override public function update(dt: Float) {}

	override public function render(engine: h3d.Engine) {}

	override public function onEvent(event: hxd.Event) {}

	override public function destroy() {}
}
